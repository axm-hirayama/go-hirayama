package router

import (
	"database/sql"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"../controller"
)

type Router struct {
	Conn *sql.DB
}

func (router Router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	var c controller.ControllerInterface
	path := req.URL.Path
	if path == "/cafes" {
		c = controller.NewCafesController(router.Conn)
	} else if matched, _ := regexp.MatchString(`^/cafes/\d+$`, path); matched {
		id, _ := strconv.Atoi(strings.Replace(req.URL.Path, "/cafes/", "", 1))
		c = controller.NewCafeController(router.Conn, id)
	} else {
		w.WriteHeader(404)
		return
	}

	switch req.Method {
	case "GET":
		c.Get(w)
		return
	case "POST":
		c.Post(w, req)
		return
	case "PUT":
		c.Put(w, req)
		return
	case "DELETE":
		c.Delete(w)
		return
	default:
		w.WriteHeader(405)
		return
	}
}
