package db

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func NewMysqlConn() *sql.DB {
	db, err := sql.Open("mysql", "root:PassWord@0306@tcp(10.10.10.200:3306)/training")
	if err != nil {
		panic(err.Error())
	}
	return db
}
