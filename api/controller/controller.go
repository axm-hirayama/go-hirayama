package controller

import (
	"net/http"
)

type ControllerInterface interface {
	Get(http.ResponseWriter)
	Post(http.ResponseWriter, *http.Request)
	Put(http.ResponseWriter, *http.Request)
	Delete(http.ResponseWriter)
}

type Controller struct{}

func (c *Controller) Get(w http.ResponseWriter) {
	w.WriteHeader(405)
	return
}

func (c *Controller) Post(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(405)
	return
}

func (c *Controller) Put(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(405)
	return
}

func (c *Controller) Delete(w http.ResponseWriter) {
	w.WriteHeader(405)
	return
}
