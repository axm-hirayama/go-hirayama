package controller

import (
	"database/sql"
	"encoding/json"
	"net/http"

	"../repository"
	"./controllerutil"
	_ "github.com/go-sql-driver/mysql"
)

type CafeController struct {
	Controller
	Repo repository.CafeRepository
	Id   int
}

func NewCafeController(conn *sql.DB, id int) *CafeController {
	return &CafeController{Controller: Controller{}, Repo: repository.CafeRepository{Conn: conn}, Id: id}
}

func (c *CafeController) Get(w http.ResponseWriter) {
	cafe, err := c.Repo.FetchById(c.Id)

	if err != nil {
		w.WriteHeader(500)
		return
	}

	if cafe.Id == 0 {
		w.WriteHeader(404)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(cafe)
}

func (c *CafeController) Put(w http.ResponseWriter, r *http.Request) {
	post, err := controllerutil.PostParse(r)
	if err != nil {
		if post == nil {
			w.WriteHeader(400)
		} else {
			w.WriteHeader(500)
		}
		return
	}

	cafe, err := c.Repo.Update(c.Id, post.Name)

	if err != nil {
		w.WriteHeader(500)
		return
	}

	if cafe.Id == 0 {
		w.WriteHeader(404)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(cafe)
}

func (c *CafeController) Delete(w http.ResponseWriter) {
	affected, err := c.Repo.Delete(c.Id)

	if err != nil {
		w.WriteHeader(500)
		return
	}

	if affected == 0 {
		w.WriteHeader(404)
		return
	}

	w.WriteHeader(204)
}
