package controller

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"

	"../repository"
	"./controllerutil"
	_ "github.com/go-sql-driver/mysql"
)

type CafesController struct {
	Controller
	Repo repository.CafeRepository
}

func NewCafesController(conn *sql.DB) *CafesController {
	return &CafesController{Controller: Controller{}, Repo: repository.CafeRepository{Conn: conn}}
}

type CafesPostParam struct {
	Name string `json:"name"`
}

func (c *CafesController) Get(w http.ResponseWriter) {
	cafes, err := c.Repo.FetchAll()

	if err != nil {
		w.WriteHeader(500)
		return
	}

	if len(cafes) == 0 {
		w.WriteHeader(404)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(cafes)
	return
}

func (c *CafesController) Post(w http.ResponseWriter, r *http.Request) {
	post, err := controllerutil.PostParse(r)
	if err != nil {
		if post == nil {
			w.WriteHeader(400)
		} else {
			w.WriteHeader(500)
		}
		return
	}

	cafe, err := c.Repo.Insert(post.Name)

	if err != nil {
		w.WriteHeader(500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Location", "https://10.10.10.10/cafes/"+strconv.Itoa(cafe.Id))
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(cafe)
	return
}
