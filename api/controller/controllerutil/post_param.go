package controllerutil

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

type postParam struct {
	Id   int
	Name string
}

func newCafesPostParam(bytes []byte) (*postParam, error) {
	var post postParam
	json.Unmarshal(bytes, &post)
	if post.Name == "" {
		return &postParam{}, errors.New("Invalid Params")
	}
	return &postParam{Name: post.Name}, nil
}

func PostParse(r *http.Request) (*postParam, error) {
	bytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	postParam, err := newCafesPostParam(bytes)
	return postParam, err
}
