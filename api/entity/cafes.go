package entity

type Cafe struct {
	Id   int    `json:"id" db:"id"`
	Name string `json:"name" db:"name"`
}

type Cafes []Cafe
