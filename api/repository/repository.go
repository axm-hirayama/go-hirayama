package repository

import (
	"database/sql"

	"../entity"
	_ "github.com/go-sql-driver/mysql"
)

type CafeRepository struct {
	Conn *sql.DB
}

func (r *CafeRepository) FetchAll() (entity.Cafes, error) {
	var cafes entity.Cafes

	rows, err := r.Conn.Query("SELECT id, name FROM go_cafes")
	if err != nil {
		if err == sql.ErrNoRows {
			return entity.Cafes{}, nil
		}
		return entity.Cafes{}, err
	}
	defer rows.Close()

	for rows.Next() {
		cafe := entity.Cafe{}
		err := rows.Scan(&cafe.Id, &cafe.Name)
		if err != nil {
			return entity.Cafes{}, err
		}
		cafes = append(cafes, cafe)
	}
	return cafes, nil
}

func (r *CafeRepository) Insert(name string) (entity.Cafe, error) {
	result, err := r.Conn.Exec("INSERT INTO go_cafes SET name = ?", name)
	if err != nil {
		return entity.Cafe{}, err
	}

	id, _ := result.LastInsertId()
	return entity.Cafe{Id: int(id), Name: name}, nil
}

func (r *CafeRepository) FetchById(id int) (entity.Cafe, error) {
	cafe := entity.Cafe{Id: id}

	err := r.Conn.QueryRow("SELECT name FROM go_cafes WHERE id = ?", id).Scan(&cafe.Name)
	if err != nil {
		if err == sql.ErrNoRows {
			return entity.Cafe{}, nil
		}
		return cafe, err
	}

	return cafe, nil
}

func (r *CafeRepository) Update(id int, name string) (entity.Cafe, error) {
	res, err := r.Conn.Exec("UPDATE go_cafes SET name = ? WHERE id = ?", name, id)

	if err != nil {
		return entity.Cafe{}, err
	}

	if num, _ := res.RowsAffected(); num < 1 {
		return entity.Cafe{}, nil
	}

	return entity.Cafe{Id: id, Name: name}, nil
}

func (r *CafeRepository) Delete(id int) (int64, error) {
	res, err := r.Conn.Exec("DELETE FROM go_cafes WHERE id = ?", id)
	if err != nil {
		return 0, err
	}

	num, _ := res.RowsAffected()
	return num, nil
}
