package main

import (
	"net/http"

	"./db"
	"./router"
)

func main() {
	conn := db.NewMysqlConn()
	defer conn.Close()

	router := router.Router{Conn: conn}

	http.Handle("/", router)
	http.ListenAndServe(":8080", nil)
}
